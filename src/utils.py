# utils.py
#
# Copyright 2023 Hemish
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from xdg.DesktopEntry import DesktopEntry
from xdg import BaseDirectory
from os.path import join
from os import listdir

bd = BaseDirectory

autostart_dir = join(bd.xdg_config_home, "autostart")

def list_autostart_apps():
    return map(lambda x: join(autostart_dir, x), listdir(autostart_dir))

def process_desktop_file(full_path):
    d = DesktopEntry(full_path)
    return {"title": d.getName(), "subtitle": d.getComment(), "icon": d.getIcon()}