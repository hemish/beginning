# window.py
#
# Copyright 2023 Hemish
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk, Gio, Adw

from .utils import process_desktop_file
from .utils import list_autostart_apps

@Gtk.Template(resource_path='/net/hemish/beginning/ui/app.ui')
class App(Adw.ActionRow):
    __gtype_name__ = 'App'

    def __init__(self, full_path):
        super().__init__()

        data = process_desktop_file(full_path)

        self.set_title(data['title'])
        self.set_subtitle(data['subtitle'])
        try:
            self.set_icon_name(data['icon'])
        except:
            pass

@Gtk.Template(resource_path='/net/hemish/beginning/ui/window.ui')
class BeginningWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'BeginningWindow'

    startup_group: Adw.PreferencesGroup = Gtk.Template.Child()
    leaflet: Adw.Leaflet = Gtk.Template.Child()
    text_view: Gtk.TextView = Gtk.Template.Child()
    preferences_page: Adw.PreferencesPage = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.create_action('add_new', self.add_new, ['<Control>n'])
        self.create_action('reload', self.reload, ['<Control>r'])
        self.create_action('go_back_to_main_view', lambda *args: self.leaflet.set_visible_child_name('main_view'))
        self.load()
    
    def add_new(self, app):
        pass

    def load(self):
        for path in list_autostart_apps():
            self.startup_group.add(App(path))

    def reload(self, *args):
        self.preferences_page.remove(self.startup_group)
        self.startup_group = Adw.PreferencesGroup()
        self.preferences_page.add(self.startup_group)
        self.load()
        


    def create_action(self, name, callback, shortcuts=None):
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.get_application().set_accels_for_action(f"win.{name}", shortcuts)